package ir.hamedrajabpour.fish.Share

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

import ir.hamedrajabpour.fish.R
import ir.hamedrajabpour.fish.Share.Camera.CameraShareFragment
import ir.hamedrajabpour.fish.Share.Gallery.GalleryShareFragment
import ir.hamedrajabpour.fish.Share.Video.VideoShareFragment
import ir.hamedrajabpour.fish.Util.ViewPagerAdapter

class ShareFragment : Fragment() {

    companion object {
        fun newInstance() = ShareFragment()
    }

    private lateinit var viewModel: ShareViewModel
    //    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout: TabLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.share_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ShareViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        viewPager = view.findViewById(R.id.ShareViewPager) as ViewPager
//
//        setupViewPager(viewPager)
//
//        viewPager.setOnClickListener {
//
//        }
        tabLayout = view.findViewById(R.id.TabShareFragment) as TabLayout
//        tabLayout.setupWithViewPager(viewPager)

        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.Share_Host_Fragment, GalleryShareFragment())
        transaction.addToBackStack(null)
        transaction.commit()


        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when {
                    tabLayout.selectedTabPosition == 0 -> {
                        val transaction = activity!!.supportFragmentManager.beginTransaction()
                        transaction.replace(R.id.Share_Host_Fragment, GalleryShareFragment())
                        transaction.addToBackStack(null)
                        transaction.commit()
                    }
                    tabLayout.selectedTabPosition == 1 -> {
                        val transaction = activity!!.supportFragmentManager.beginTransaction()
                        transaction.replace(R.id.Share_Host_Fragment, CameraShareFragment())
                        transaction.addToBackStack(null)
                        transaction.commit()
                    }
                    tabLayout.selectedTabPosition == 2 -> {
                        val transaction = activity!!.supportFragmentManager.beginTransaction()
                        transaction.replace(R.id.Share_Host_Fragment, VideoShareFragment())
                        transaction.addToBackStack(null)
                        transaction.commit()
                    }
                }

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })


    }

    private fun setupViewPager(viewPager: ViewPager?) {
        val adapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        adapter.addFragment(GalleryShareFragment(), "گالری")
        adapter.addFragment(CameraShareFragment(), "دوربین")
        adapter.addFragment(VideoShareFragment(), "ویدئو")
        viewPager?.adapter = adapter

    }


}
